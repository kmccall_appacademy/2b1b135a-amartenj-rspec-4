class Dictionary
  attr_reader :entries

  def initialize
    @entries = {}
  end

  def add(entry)
    if entry.is_a?(String)
      @entries[entry] = nil
    elsif entry.is_a?(Hash)
      @entries[entry.keys.first] = entry.values.first
    end
  end

  def keywords
    @entries.keys.sort
  end

  def include?(key)
    @entries.key?(key)
  end

  def find(str)
    @entries.select { |k, v| k.start_with?(str) }
  end

  def printable
    print = []
    keywords.each { |key| print << "[#{key}] \"#{@entries[key]}\"" }
    print.join("\n")
  end
end
