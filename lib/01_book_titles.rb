class Book
  def initialize
  end

  def title
    @title
  end

  def title=(title)
    exceptions = ["the", "a", "an", "and", "in", "of"]

    words = title.split
    words.map!.with_index do |word, idx|
      if idx == 0 || !exceptions.include?(word)
        word.capitalize
      else
        word
      end
    end
    @title = words.join(" ")
  end
end
