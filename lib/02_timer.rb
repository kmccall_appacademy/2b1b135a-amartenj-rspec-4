class Timer
  attr_accessor :seconds

  def initialize
    @seconds = 0
  end

  def time_string
    "#{pad(@seconds/3600)}:#{pad(@seconds/60 % 60)}:#{pad(@seconds % 60)}"
  end

  def pad(number)
    if number < 10
      "0#{number}"
    else
      number.to_s
    end
  end
end
