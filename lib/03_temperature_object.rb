class Temperature
  def initialize(options)
    p options
    @f = options[:f]
    @c = options[:c]
  end

  def in_fahrenheit
    if @f.nil?
      (@c * 9 / 5.0) + 32
    else
      @f
    end
  end

  def in_celsius
    if @c.nil?
      (@f - 32) * 5 / 9.0
    else
      @c
    end
  end

  def self.from_celsius(cel)
    Temperature.new(c: cel)
  end

  def self.from_fahrenheit(far)
    Temperature.new(f: far)
  end
end

class Celsius < Temperature
  def initialize(cel)
    super(c: cel)
  end
end

class Fahrenheit < Temperature
  def initialize(far)
    super(f: far)
  end
end
